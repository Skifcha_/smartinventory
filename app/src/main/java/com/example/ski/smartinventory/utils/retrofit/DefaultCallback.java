package com.example.ski.smartinventory.utils.retrofit;

import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.util.Log;

import com.example.ski.smartinventory.ApplicationWrapper;
import com.example.ski.smartinventory.R;
import com.example.ski.smartinventory.base_arch.presentation.view.BaseView;
import com.example.ski.smartinventory.utils.NotificationDialog;
import com.google.gson.Gson;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Spudi on 18.08.2016.
 * whoose.daddy@gmail.com
 */
public abstract class DefaultCallback<T> implements Callback<T> {

    private WeakReference<BaseView> mViewRef;

    public DefaultCallback() {
    }

    public DefaultCallback(BaseView mViewRef) {
        this.mViewRef = new WeakReference<>(mViewRef);
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        handleServerError(response);
        onResponse(response);
    }

    public abstract void onResponse(Response<T> response);

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (mViewRef != null && mViewRef.get() != null)
            mViewRef.get().hideProgressView();
        //output in dialog
        String message;
        if (t instanceof IOException)
            message = getString(R.string.retrofitErrorApiNoInternet);
        else message = t.getMessage();
        showDialog(message);
        //log output
        try {
            Log.e("DEFAULT_CALLBACK", String.format("onFailure: %s", call.request().url().toString()), t);
        } catch (Exception e) {
            Log.e("DEFAULT_CALLBACK", "onFailure: ", t);
        }
    }

    private void handleServerError(Response<T> response) {
        switch (response.code()) {
//            case HttpURLConnection.HTTP_NOT_FOUND:
            case HttpURLConnection.HTTP_UNAVAILABLE:
            case HttpURLConnection.HTTP_INTERNAL_ERROR:
                showDialog(getString(R.string.retrofitErrorApi500));
                break;
            default: {
                String errorMessage = generateMeaningfulMessage(response);
                showDialog(errorMessage);
                Log.i("handleServerError: %s", errorMessage);
            }
        }
    }

    private void showDialog(String message) {
        if (mViewRef == null || mViewRef.get() == null) {
            Log.i("DEFAULT_CALLBACK", "showDialog: mViewRef == NULL");
            return;
        }
        if (TextUtils.isEmpty(message)) {
            Log.i("DEFAULT_CALLBACK", "showDialog: message is EMPTY");
            return;
        }
        try {
            NotificationDialog dialog = new NotificationDialog(mViewRef.get().getActContext());
            dialog.setTitle(R.string.app_name);
            dialog.setMessage(message);
            dialog.setCancelable(true);
            dialog.notifyUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String generateMeaningfulMessage(Response<T> response) {
        try {
            String message = response.errorBody() != null ? response.errorBody().string() : "";
            if (!TextUtils.isEmpty(message)) {
                BaseError error = new Gson().fromJson(message, BaseError.class);
                if (error != null) return error.getFullError();
            }
            return message;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getString(@StringRes int id) {
        return ApplicationWrapper.getContext().getString(id);
    }
}
