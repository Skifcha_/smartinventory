package com.example.ski.smartinventory.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ski.smartinventory.ApplicationWrapper;
import com.example.ski.smartinventory.R;
import com.example.ski.smartinventory.api.ListItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.example.ski.smartinventory.ApplicationWrapper.getContext;


public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {

    private List<ListItem> list;
    private Context context;
    private String email = "";
    private String subject = "";
    private String message = "";

    public ListAdapter(List<ListItem> list, Context context) {
        this.list = list;
        this.context = context;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        final Uri uri = Uri.parse((list.get(position).getPhotoId()));
        holder.imageItem.setImageURI(uri);
        holder.itemLocation.setText(list.get(position).getLocation());
        holder.itemID.setText(list.get(position).getToken());
        if (list.get(position).getStatus().equals("good")) {
            holder.imageStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ok));
        } else {
            holder.imageStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.warning));
        }

        holder.imageStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Change item status")
                        .setPositiveButton(" ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                changeList("good", position);
                                list.get(position).setStatus("good");
                                holder.imageStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ok));
                            }
                        })

                        .setNegativeButton(" ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                changeList("bad", position);
                                list.get(position).setStatus("bad");
                                holder.imageStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.warning));
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

                Button btnPos = alert.getButton(AlertDialog.BUTTON_POSITIVE);
                LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) btnPos.getLayoutParams();
                positiveButtonLL.width = ViewGroup.LayoutParams.MATCH_PARENT;
                positiveButtonLL.weight = 1;
                btnPos.setLayoutParams(positiveButtonLL);

                Drawable good = ContextCompat.getDrawable(context, R.drawable.ok);
                btnPos.setCompoundDrawablesWithIntrinsicBounds(resize(good), null, null, null);
                btnPos.setText("");

                Button btnNeg = alert.getButton(AlertDialog.BUTTON_NEGATIVE);
                LinearLayout.LayoutParams negativeLL = (LinearLayout.LayoutParams) btnNeg.getLayoutParams();
                negativeLL.width = ViewGroup.LayoutParams.MATCH_PARENT;
                negativeLL.weight = 1;
                btnNeg.setLayoutParams(negativeLL);

                Drawable bad = ContextCompat.getDrawable(context, R.drawable.warning);
                btnNeg.setCompoundDrawablesWithIntrinsicBounds(resize(bad), null, null, null);
                btnNeg.setText("");

            }
        });
        holder.imageSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "sending...", Toast.LENGTH_LONG).show();
                try {

                    email = SPrefs.getInstance().getLogin();
                    subject = PrefConstantaz.SUBJECT;
                    message = "Item ID: " + list.get(position).getToken() + "\n"
                            + "Location: " + list.get(position).getLocation() + "\n";
                    if (list.get(position).getStatus().equals("good"))
                        message += "Status: good" + "\n";
                    else
                        message += "Status: damaged" + "\n";

                    Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

                    emailIntent.setType("plain/text");
                    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
                    String pathofBmp = MediaStore.Images.Media.insertImage(context.getContentResolver(),
                            ((BitmapDrawable) holder.imageItem.getDrawable()).getBitmap(), "item", null);
                    Uri bmpUri = Uri.parse(pathofBmp);

                    if (bmpUri != null) {
                        emailIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                    }

                    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);
                    context.startActivity(Intent.createChooser(emailIntent, "Sending email..."));
                } catch (Throwable t) {
                    Toast.makeText(context, "Request failed try again: " + t.toString(), Toast.LENGTH_LONG).show();
                }


            }
        });
    }

    private Drawable resize(Drawable image) {
        Bitmap b = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 100, 100, false);
        return new BitmapDrawable(ApplicationWrapper.getRes(), bitmapResized);
    }


    private void changeList(String status, int position){
        List<ListItem> list = new ArrayList<>();
        try {
            list = (List<ListItem>) InternalStorage.readObject(getContext(), PrefConstantaz.KEY);
            Log.d("logss", "+++" + "lest ---  " + list.toString());
            list.get(position).setStatus(status);
            InternalStorage.writeObject(getContext(), PrefConstantaz.KEY, list);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageItem;
        ImageView imageStatus;
        ImageView imageSend;
        TextView itemID;
        TextView itemLocation;

        MyViewHolder(View view) {
            super(view);
            imageItem = (ImageView) view.findViewById(R.id.itemImage);
            imageStatus = (ImageView) view.findViewById(R.id.itemState);
            imageSend = (ImageView) view.findViewById(R.id.itemSendImage);
            itemID = (TextView) view.findViewById(R.id.itemID);
            itemLocation = (TextView) view.findViewById(R.id.itemLocation);

        }
    }
}
