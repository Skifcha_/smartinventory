package com.example.ski.smartinventory.choice;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import com.example.ski.smartinventory.R;
import com.example.ski.smartinventory.base_arch.activity.BaseActivity;
import com.example.ski.smartinventory.base_arch.fragment.BaseFragment;
import com.example.ski.smartinventory.camera.CameraFragment;
import com.example.ski.smartinventory.choice.presentation.presenter.ChoicePresenter;
import com.example.ski.smartinventory.choice.presentation.view.ChoiceView;
import com.example.ski.smartinventory.list.ListFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class ChoiceFragment extends BaseFragment<ChoicePresenter> implements ChoiceView {

    private Bitmap image;

    @OnClick(R.id.buttonList)
    public void list() {
        showListFragment();

    }

    @OnClick(R.id.buttonNewElement)
    public void newElement() {
        startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            image = (Bitmap) extras.get("data");
            showCameraFragment();
        }
    }

    public ChoiceFragment() {
        // Required empty public constructor
    }

    public static ChoiceFragment newInstance() {
        Bundle args = new Bundle();
        ChoiceFragment fragment = new ChoiceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected ChoicePresenter createPresenter() {
        return new ChoicePresenter(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_choice;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void findViews(View rootView) {
        ButterKnife.bind(this, rootView);
        super.findViews(rootView);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public BaseActivity getActContext() {
        return (BaseActivity) getActivity();
    }

    private void showCameraFragment() {
        CameraFragment cameraFragment = new CameraFragment().newInstance();
        cameraFragment.setBitmap(image);
        replaceFragment(R.id.container, cameraFragment, "CameraFragment");

    }

    private void showListFragment() {
        replaceFragment(R.id.container, ListFragment.newInstance(), "ListFragment");

    }
}
