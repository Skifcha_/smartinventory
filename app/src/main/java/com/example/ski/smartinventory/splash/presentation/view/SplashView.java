package com.example.ski.smartinventory.splash.presentation.view;


import com.example.ski.smartinventory.base_arch.presentation.view.BaseView;

public interface SplashView extends BaseView {

    void openMain();

    void  openAuth();
}
