package com.example.ski.smartinventory.api;

import java.io.Serializable;


public class ListItem implements Serializable {

    private String photoId = "";
    private String token = "";
    private String status = "";
    private String location = "";

    public ListItem (String photoId, String token, String status, String location){
        this.photoId = photoId;
        this.token = token;
        this.status = status;
        this.location = location;
    }
    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus(){
        return status;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }
}
