package com.example.ski.smartinventory.choice.presentation.presenter;

import com.example.ski.smartinventory.base_arch.presentation.presenter.BasePresenter;
import com.example.ski.smartinventory.choice.presentation.view.ChoiceView;

public class ChoicePresenter extends BasePresenter<ChoiceView> {

    public ChoicePresenter(ChoiceView view) {
        super(view);
    }

    @Override
    public void init() {
    }
}
