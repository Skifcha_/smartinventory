package com.example.ski.smartinventory.utils;

public final class PrefConstantaz {

    public static final String LIST = "LIST";

    public static final String LOGIN = "LOGIN";

    public static final String FILE_NAME = "LIST_OF_ITEMS";

    public static final String KEY = "LIST_KEY";

    public static final String SUBJECT = "SMARTInventory";

}
