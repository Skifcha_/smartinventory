package com.example.ski.smartinventory;

import android.app.Application;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import com.example.ski.smartinventory.utils.SPrefs;

public class ApplicationWrapper extends Application {

    public static final String TAG = ApplicationWrapper.class.getSimpleName();

    private volatile static ApplicationWrapper instance;

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static Resources getRes() {
        return instance.getResources();
    }

    public static SharedPreferences getPrefs() {
        return PreferenceManager.getDefaultSharedPreferences(getContext());
    }
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

}
