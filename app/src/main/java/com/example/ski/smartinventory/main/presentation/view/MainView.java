package com.example.ski.smartinventory.main.presentation.view;

import com.example.ski.smartinventory.base_arch.presentation.view.BaseView;

public interface MainView extends BaseView {

    void showChoiceFragment();

}
