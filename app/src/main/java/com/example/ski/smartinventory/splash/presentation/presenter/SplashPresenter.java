package com.example.ski.smartinventory.splash.presentation.presenter;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import com.example.ski.smartinventory.base_arch.presentation.presenter.BasePresenter;
import com.example.ski.smartinventory.splash.presentation.view.SplashView;
import com.example.ski.smartinventory.utils.SPrefs;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

public class SplashPresenter extends BasePresenter<SplashView> {


    private static final int SPLASH_TIME = 500;

    String login;

    public SplashPresenter(SplashView view) {
        super(view);
    }

    @Override
    public void init() {

        login = SPrefs.getInstance().getLogin();

//        checkPermission();
//        delay();
    }

    private void checkPermission(){
        Dexter.checkPermission(new PermissionListener() {
            @Override public void onPermissionGranted(PermissionGrantedResponse response) {
                delay();
            }
            @Override public void onPermissionDenied(PermissionDeniedResponse response) {
                new AlertDialog.Builder(view.getActContext())
                        .setTitle("Camera Permission")
                        .setMessage("Do you want grant Camera Permission or ext?")
                        .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                view.getActContext().finish();
                            }
                        })
                        .setNegativeButton("grant", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            checkPermission();
                            }
                        })
                        .show();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                new AlertDialog.Builder(view.getActContext())
                        .setTitle("Camera Permission")
                        .setMessage("Pls go to app settings and grant Camera Permission or exit")
                        .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                view.getActContext().finish();
                            }
                        })
                        .setNegativeButton("settings", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                startInstalledAppDetailsActivity(view.getActContext());
                            }
                        })
                        .show();
            }
        }, Manifest.permission.CAMERA);

    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }
    private void delay() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (login.equals(""))
                    view.openAuth();
                else
                    view.openMain();
            }
        }, SPLASH_TIME);
    }
}
