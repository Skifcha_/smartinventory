package com.example.ski.smartinventory.camera;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.ski.smartinventory.R;
import com.example.ski.smartinventory.api.ListItem;
import com.example.ski.smartinventory.base_arch.activity.BaseActivity;
import com.example.ski.smartinventory.base_arch.fragment.BaseFragment;
import com.example.ski.smartinventory.camera.presentation.presenter.CameraPresenter;
import com.example.ski.smartinventory.camera.presentation.view.CameraView;
import com.example.ski.smartinventory.choice.ChoiceFragment;
import com.example.ski.smartinventory.utils.InternalStorage;
import com.example.ski.smartinventory.utils.PrefConstantaz;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CameraFragment extends BaseFragment<CameraPresenter> implements CameraView {

    @BindView(R.id.newImage)
    ImageView imageView;

    @BindView(R.id.spinnerCamera)
    Spinner spinner;

    @BindView(R.id.etCamera)
    EditText editText;

    @OnClick(R.id.buttonGood)
    public void good() {
        newItem("good");
    }

    @OnClick(R.id.buttonBad)
    public void bad() {
        newItem("bad");
    }

    private Bitmap image;


    public void newItem(String state) {


        Log.d("logss", "+++" + "new " + spinner.getSelectedItem().toString());
        List<ListItem> list = new ArrayList<>();
        String itemID;
        String location;
        String path;
        if (editText.getText().toString().equals("") ) {
            Toast.makeText(getContext(), " Fill item ID field", Toast.LENGTH_LONG).show();
        } else {

            itemID = editText.getText().toString();
            location = spinner.getSelectedItem().toString();
            path = saveImage(image);
            try {
                list = (List<ListItem>) InternalStorage.readObject(getContext(), PrefConstantaz.KEY);
                Log.d("logss", "+++" + "lest ---  " + list.toString());

                list.add(new ListItem(path, itemID, state, location));
                InternalStorage.writeObject(getContext(), PrefConstantaz.KEY, list);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

            if (list.isEmpty()) {
                list.add(new ListItem(path, itemID, state, location));
                try {
                    InternalStorage.writeObject(getContext(), PrefConstantaz.KEY, list);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            showChoiceFragment();
            closeKeyboard();

        }


    }

    public CameraFragment() {
        // Required empty public constructor
    }

    public static CameraFragment newInstance() {
        Bundle args = new Bundle();
        CameraFragment fragment = new CameraFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setBitmap(Bitmap image) {
        this.image = image;

    }

    @Override
    protected CameraPresenter createPresenter() {
        return new CameraPresenter(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_camera;
    }

    @Override
    protected void findViews(View rootView) {
        ButterKnife.bind(this, rootView);
        super.findViews(rootView);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        imageView.setImageBitmap(image);
    }

    public String saveImage(Bitmap image) {

        ContextWrapper wrapper = new ContextWrapper(getContext());
        File file = wrapper.getDir("Images", Context.MODE_PRIVATE);
        String imageName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        file = new File(file, imageName);
        try {
            OutputStream outputStream = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return file.getAbsolutePath();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public BaseActivity getActContext() {
        return (BaseActivity) getActivity();
    }

    private void showChoiceFragment() {
        replaceFragment(R.id.container, ChoiceFragment.newInstance(), "ChoiceFragment");

    }
}
