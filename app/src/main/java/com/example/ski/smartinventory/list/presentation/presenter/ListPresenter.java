package com.example.ski.smartinventory.list.presentation.presenter;

import com.example.ski.smartinventory.base_arch.presentation.presenter.BasePresenter;
import com.example.ski.smartinventory.list.presentation.view.ListView;

public class ListPresenter extends BasePresenter<ListView> {

    public ListPresenter(ListView view) {
        super(view);
    }

    @Override
    public void init() {
    }
}
