package com.example.ski.smartinventory.base_arch.presentation.presenter;

import com.example.ski.smartinventory.base_arch.presentation.view.BaseView;

public abstract class BasePresenter<T extends BaseView> {

    protected T view;

    public BasePresenter(T view) {
        this.view = view;
    }

    public abstract void init();

}
