package com.example.ski.smartinventory.camera.presentation.presenter;

import com.example.ski.smartinventory.base_arch.presentation.presenter.BasePresenter;
import com.example.ski.smartinventory.camera.presentation.view.CameraView;

public class CameraPresenter extends BasePresenter<CameraView> {

    public CameraPresenter(CameraView view) {
        super(view);
    }

    @Override
    public void init() {
    }
}
