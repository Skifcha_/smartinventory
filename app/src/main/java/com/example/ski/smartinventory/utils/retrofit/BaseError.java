package com.example.ski.smartinventory.utils.retrofit;

import android.text.TextUtils;

import java.util.List;


public class BaseError {

    private String error;
    private List<String> errors;

    public String getError() {
        return error;
    }

    public List<String> getErrors() {
        return errors;
    }

    public String getFullError() {
        String message = "";
        if (!TextUtils.isEmpty(error)) message += error + " ";
        if (errors != null && !errors.isEmpty()) {
            for (String s : errors) message += s + " ";
        }
        return message;
    }
}
