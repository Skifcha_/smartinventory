package com.example.ski.smartinventory.list;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.ski.smartinventory.R;
import com.example.ski.smartinventory.api.ListItem;
import com.example.ski.smartinventory.base_arch.activity.BaseActivity;
import com.example.ski.smartinventory.base_arch.fragment.BaseFragment;
import com.example.ski.smartinventory.list.presentation.presenter.ListPresenter;
import com.example.ski.smartinventory.list.presentation.view.ListView;
import com.example.ski.smartinventory.utils.InternalStorage;
import com.example.ski.smartinventory.utils.ListAdapter;
import com.example.ski.smartinventory.utils.PrefConstantaz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListFragment extends BaseFragment<ListPresenter> implements ListView {

    @BindView(R.id.recyclerViewList)
    RecyclerView recyclerView;

    @BindView(R.id.textViewList)
    TextView textViewList;

    List<ListItem> list;

    public ListFragment() {
        // Required empty public constructor
    }

    public static ListFragment newInstance() {
        Bundle args = new Bundle();
        ListFragment fragment = new ListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected ListPresenter createPresenter() {
        return new ListPresenter(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list;
    }

    @Override
    protected void findViews(View rootView) {
        ButterKnife.bind(this, rootView);
        super.findViews(rootView);
        list = new ArrayList<>();
        try {
            list = (List<ListItem>) InternalStorage.readObject(getContext(), PrefConstantaz.KEY);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (list.isEmpty()) {
            textViewList.setVisibility(View.VISIBLE);
        } else {
            ListAdapter listAdapter = new ListAdapter(list, getContext());
            recyclerView.setAdapter(listAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        }

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public BaseActivity getActContext() {
        return (BaseActivity) getActivity();
    }
}
