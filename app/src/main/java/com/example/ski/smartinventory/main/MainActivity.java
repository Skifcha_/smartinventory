package com.example.ski.smartinventory.main;

import android.content.Intent;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.ski.smartinventory.base_arch.activity.BaseActivity;
import com.example.ski.smartinventory.camera.CameraFragment;
import com.example.ski.smartinventory.choice.ChoiceFragment;
import com.example.ski.smartinventory.login.LoginActivity;
import com.example.ski.smartinventory.main.presentation.view.MainView;
import com.example.ski.smartinventory.main.presentation.presenter.MainPresenter;
import com.example.ski.smartinventory.R;
import com.example.ski.smartinventory.utils.SPrefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class MainActivity extends BaseActivity<MainPresenter> implements MainView {


    @BindView(R.id.container)
    FrameLayout container;

    @BindView(R.id.toolbarTitle)
    TextView toolbarTitle;


    @OnClick(R.id.toolbarMenuContainer)
    public void logout(){
        SPrefs.getInstance().setLogin("");
        startActivity(new Intent(getBaseContext(), LoginActivity.class));
        finish();
    }

    @Override
    protected MainPresenter createPresenter() {
        return new MainPresenter(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void findViews()
    {
        super.findViews();
        ButterKnife.bind(this);
        initToolbar();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        ChoiceFragment myFragment = (ChoiceFragment) getSupportFragmentManager().findFragmentByTag("ChoiceFragment");
        if (myFragment != null && myFragment.isVisible()) {
            super.onBackPressed();

        } else {
            showChoiceFragment();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    private void  initToolbar(){
        toolbarTitle.setText(SPrefs.getInstance().getLogin());
    }


    @Override
    public void showChoiceFragment() {
        replaceFragment(R.id.container, ChoiceFragment.newInstance(), "ChoiceFragment");

    }
}
