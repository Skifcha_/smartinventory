package com.example.ski.smartinventory.main.presentation.presenter;

import com.example.ski.smartinventory.base_arch.presentation.presenter.BasePresenter;
import com.example.ski.smartinventory.main.presentation.view.MainView;

public class MainPresenter extends BasePresenter<MainView> {

    public MainPresenter(MainView view) {
        super(view);
    }

    @Override
    public void init() {
        view.showChoiceFragment();
    }
}
