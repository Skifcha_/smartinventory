package com.example.ski.smartinventory.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.ski.smartinventory.ApplicationWrapper;

public class SPrefs {

    private static SPrefs instance;
    private SharedPreferences preferences;

    public SPrefs() {
        preferences = PreferenceManager.getDefaultSharedPreferences(ApplicationWrapper.getContext());
    }

    public static SPrefs getInstance() {
        if (instance == null)
            instance = new SPrefs();
        return instance;
    }

    public void clearAllPreferences() {
        preferences.edit().clear().apply();
    }



    public  void  setList(){
    }

    public  void setLogin(String login){
        preferences.edit().putString(PrefConstantaz.LOGIN, login).apply();

        }

    public String getLogin() {
        return preferences.getString(PrefConstantaz.LOGIN, "");
    }




}
