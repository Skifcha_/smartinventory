package com.example.ski.smartinventory.login;

import android.content.Intent;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ski.smartinventory.R;
import com.example.ski.smartinventory.base_arch.activity.BaseActivity;
import com.example.ski.smartinventory.login.presentation.presenter.LoginPresenter;
import com.example.ski.smartinventory.login.presentation.view.LoginView;
import com.example.ski.smartinventory.main.MainActivity;
import com.example.ski.smartinventory.utils.SPrefs;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity<LoginPresenter> implements LoginView {

    @BindView(R.id.etLogin)
    EditText etLogin;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @OnClick(R.id.buttonLogin)
    public void login() {

        if (!String.valueOf(etPassword.getText().toString()).equals("Inventory2016")) {
            Toast.makeText(this, "Wrong password", Toast.LENGTH_LONG).show();
        } else if (!isValidEmailID(etLogin.getText().toString())) {
            Toast.makeText(this, "Wrong email", Toast.LENGTH_LONG).show();
        } else {
            SPrefs.getInstance().setLogin(etLogin.getText().toString());
            startActivity(new Intent(this, MainActivity.class));
            closeKeyboard();
            finish();
        }
    }

    private boolean isValidEmailID(String email) {
        String PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    protected LoginPresenter createPresenter() {
        return new LoginPresenter(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void findViews() {
        ButterKnife.bind(this);
        super.findViews();


    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
