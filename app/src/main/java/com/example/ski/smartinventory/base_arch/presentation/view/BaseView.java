package com.example.ski.smartinventory.base_arch.presentation.view;

import com.example.ski.smartinventory.base_arch.activity.BaseActivity;

public interface BaseView {

    void showProgressDialog();

    void hideProgressDialog();

    void showProgressView();

    void hideProgressView();

    BaseActivity getActContext();

    void showNotificationDialog(String title, String message);

    /**
     * !ADD THIS TO YOUR TOP-
     * classpath 'com.neenbedankt.gradle.plugins:android-apt:1.8'
     * AND MODULE-LEVEL GRADLE FILES
     * apply plugin: 'com.neenbedankt.android-apt'
     * apt 'com.jakewharton:butterknife-compiler:8.2.1'
     * AND DELETE THIS!
     */

}
