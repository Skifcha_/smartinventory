package com.example.ski.smartinventory.login.presentation.presenter;

import com.example.ski.smartinventory.base_arch.presentation.presenter.BasePresenter;
import com.example.ski.smartinventory.login.presentation.view.LoginView;

public class LoginPresenter extends BasePresenter<LoginView> {

    public LoginPresenter(LoginView view) {
        super(view);
    }

    @Override
    public void init() {
    }
}
