package com.example.ski.smartinventory.splash;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.example.ski.smartinventory.R;
import com.example.ski.smartinventory.base_arch.activity.BaseActivity;
import com.example.ski.smartinventory.login.LoginActivity;
import com.example.ski.smartinventory.main.MainActivity;
import com.example.ski.smartinventory.splash.presentation.presenter.SplashPresenter;
import com.example.ski.smartinventory.splash.presentation.view.SplashView;
import com.example.ski.smartinventory.utils.SPrefs;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.DexterActivity;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.List;

public class SplashActivity extends BaseActivity<SplashPresenter> implements SplashView {

    private static final int SPLASH_TIME = 500;

    String login;

    @Override
    protected SplashPresenter createPresenter() {
        return new SplashPresenter(this);
    }



    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void findViews() {
        super.findViews();
        login = SPrefs.getInstance().getLogin();

        askpPrem();
    }

    public void askpPrem(){
        if ((ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED )) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {


                new AlertDialog.Builder(getActContext())
                        .setTitle("Camera Permission")
                        .setMessage("Pls go to app settings and grant Camera Permission or exit")
                        .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNegativeButton("settings", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                startInstalledAppDetailsActivity(getActContext());
                            }
                        })
                        .show();

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                        2);

            }
        } else {
            delay();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

            switch (requestCode) {
                case 2: {
                    if ((grantResults.length > 1)
                            && (grantResults[0] == PackageManager.PERMISSION_GRANTED) && (grantResults[1] == PackageManager.PERMISSION_GRANTED)) {


                        delay();
                    } else {

                        askpPrem();

                    }
                }
            }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        askpPrem();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void openAuth() {
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();

    }


//
    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivityForResult(i, 1);
    }
    private void delay() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (login.equals(""))
                    openAuth();
                else
                    openMain();
            }
        }, SPLASH_TIME);
    }
    @Override
    public void openMain() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }

}
